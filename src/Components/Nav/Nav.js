import React from 'react';
import './Nav.css';

function Nav() {
  return (
    <nav>
    <div className="container">
      <h1 className="nav-name">her lens shoots</h1>
      <div className="nav-links">
        <a href="#about">o mnie</a>
        <a href="#offer">oferta</a>
        <a href="#contact" target="_blank" className="nav-links-anchor">kontakt</a>
      </div>
    </div>
  </nav>
  )
}

export default Nav;