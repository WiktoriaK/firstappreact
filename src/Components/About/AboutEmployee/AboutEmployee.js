import React from 'react';
import './AboutEmployee.css';

function AboutEmployee({title, desc}) {
  return (
 
    
        <div className="about-employee">
          <div className="about-employee-img"></div>
          <div className="about-employee-text">
            <h2>{title}</h2>
            <p>{desc}
            </p>
          </div>
        </div>
      
  )
}

export default AboutEmployee;