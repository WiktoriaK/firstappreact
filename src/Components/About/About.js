import React from 'react';
import './About.css';
import AboutEmployee from './AboutEmployee/AboutEmployee';

function About() {
  let employees = [
    {title: "Imię Nazwisko [Dział]", desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry." },
    {title: "Imię Nazwisko [Dział]", desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry." },
    {title: "Imię Nazwisko [Dział]", desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry." },
    {title: "Imię Nazwisko [Dział]", desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry." },
  ]

  let aboutEmployees = employees.map((about, index) => <AboutEmployee key={index} title={about.title} desc={about.desc}></AboutEmployee>)

  return (
    <section className="about" id="about">
      <div className="container">
        <h1 className="dark">Nasi specjaliści</h1>
          {aboutEmployees}
      </div>
    </section>
  )
}

export default About;