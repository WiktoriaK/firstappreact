import React from 'react';
import './LandingPage.css';

function LandingPage() {
  return (
    <section className="landing-page">
    <div className="landing-page-shadow">
      <div className="container">
        <div className="landing-page-text">
          <h1>Nasza firma oferuje najwyższej jakości zdjęcia.</h1>
          <h2>Nie wierz nam na słowo - sprawdź</h2>
          <button onClick="location.href='#offer'" className="landing-page-btn">oferta</button>
        </div>
      </div>
    </div>
  </section>
  )
}

export default LandingPage;