import React from 'react';
import './Offer.css';
import OfferBox from './OfferBox/OfferBox';

function Offer() {
  let offers = [
    {title: "Usługa 1", desc: "(Nowość)" ,  isNew: true },
    {title: "Usługa 2", desc: "(wkrótce)" , isNew: false},
    {title: "Usługa 3", desc: "(wkrótce)" , isNew: false},
    {title: "Usługa 4", desc: "(wkrótce)" , isNew: false},
    {title: "Usługa 5", desc: "(wkrótce)" , isNew: false},
    {title: "Usługa 6", desc: "(wkrótce)" , isNew: false},
    {title: "Usługa 7", desc: "(wkrótce)" , isNew: false},
    {title: "Usługa 8", desc: "(wkrótce)" , isNew: false},
    {title: "Usługa 9", desc: "(wkrótce)" , isNew: false},
  ]  

  let offerBoxes = offers.map((offer, index) => {
    return (
      <OfferBox key={index} title={offer.title} desc={offer.desc} isNew={offer.isNew}></OfferBox>
      )
    })

  return (
        <section className="offer" id="offer">
            <div className="container">
                 
                    <h1>Czym zajmuje się nasza firma?</h1> 
                    <div className="offer-box-wrapper">
                        {offerBoxes}
                    </div>
            </div> 
        </section>
  )
}

export default Offer;