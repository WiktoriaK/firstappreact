import React from 'react';
import './OfferBox.css';

function OfferBox({title, desc, isNew}) {
  return (          
    <div className="offer-box">
        <div className="offer-box-content">
          { isNew && (<div class="offer-box-dot" />) }
          {title} 
          <p>{desc}</p>
        </div>
    </div>
              
  )
}

export default OfferBox;