import React from 'react';
import { FaInstagram } from 'react-icons/fa';
import { FaFacebook } from 'react-icons/fa';
import './Footer.css';


function Footer() {
  return (
    <footer className="footer">
        <div className="container">
            <div>
                <h2 className="footer-name">Her Lens Shoots - wszelkie prawa zastrzeżone, 2020</h2>
            </div>

            <div className="footer-icon">   
                <FaInstagram  />
                <FaFacebook />
            </div>
        </div>
  </footer>
  )
}

export default Footer;