import React from 'react';
import './Contact.css';


function Contact () {
 

  return (
    <section className="contact" id="contact">
      <div className="container">
        <h1 className="dark">Kontakt</h1>
        <div className="cont">
            <h2>Her Lens Shoots</h2>
            <p>Telefon: 777-777-777</p>
            <p>E-mail: camera@gmail.com</p>
            <p>Siedziba firmy: Warszawa, Aleje Jerozolimskie</p>
            <textarea name="Komentarz" cols="50" rows="10">Napisz wiadomość</textarea>
            <input type="submit" value="Wyślij"/>
        </div>
      </div>
    </section>
  )
}

export default Contact;